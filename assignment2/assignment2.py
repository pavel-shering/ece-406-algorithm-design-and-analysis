#!/usr/bin/env python3
"""
Assignment 2 Python file
Copy-and-paste your extended_euclid and modexp functions
from assignment 1
"""
import random
import math
import sys
sys.path.append('../assignment1')
from assignment1 import modexp, extended_euclid

def primality(N):
    """
    Test if a number N is prime using Fermat's little Theorem with
    ten random values of a.  If a^(N-1) mod N = 1 for all values,
    then return true.  Otherwise return false.
    Hint:  you can generate a random integer between a and b using
    random.randint(a,b).
    """
    k = 10
    a_array = []
    for i in range(0,k):
        a_array.append( random.randint(2, N - 1) )
    for a in a_array:
        if modexp(a, N-1, N) != 1:
            return False
    return True

def prime_generator(N):
    """
    This function generates a prime number <= N
    """
    for i in range(0, N*N*N*N):
        n = random.randint(N/10, N)
        if(primality(n)): return n
    return False

    # n = random.randint(N/10, N)
    # while (not primality(n)):
    #     n = random.randint(N/10, N)
    # return n


def main():
    """
    Test file for the two parts of the question
    """
    ## Exercise 1:  generating primes and RSA
    ##################
    
    p = prime_generator(10000000)
    q = prime_generator(10000000)
    d = 0;
    e = 5
    # print("q=%d, p=%d" %( q, p))

    # id d is 1 then p and q are relatively prime 
    while d != 1:
        p = prime_generator(10000000)
        if p == False: 
            print("failed to generate a prime number")
            sys.exit(0)
        q = prime_generator(10000000)
        if q == False: 
            print("failed to generate a prime number")
            sys.exit(0)

        g = (p-1) * (q-1)
        [x, y, d] = extended_euclid(g,e)

    print("p=%d, q=%d" %( p, q))
    N = p*q
    [_, private_key, _] = extended_euclid(g,e)
    print("private_key=%d" %(private_key))
    if (y < 0) : private_key += g
    print("e= %d, mult_inv=%d" %(e, N / private_key ))
    print("private_key=%d" %(private_key))
    print("e*private_key = %d and (e*private_key-1)/(p-1)(q-1)= %d" % (e*private_key, (e*private_key-1) / N ))

    x = 329415
    encoded_message = modexp(x, e, N)
    print("encoded_message=%d" % encoded_message)

    decoded_message = modexp(encoded_message, private_key, N)
    print("decoded_message=%d" % decoded_message)


if __name__ == '__main__':
    main()
