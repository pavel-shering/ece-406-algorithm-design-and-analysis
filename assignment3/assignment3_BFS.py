#!/usr/bin/env python3
"""
ECE406
Python file for assignment 3, Exercise 5

we will use the queue module to implement a FIFO queue.
- you can create a queue with Q = queue.queue(max_size)
- you can inject v onto the queue with Q.put(v)
- you can eject from the queue with Q.get()
"""
import queue
import numpy as np

def reverse(G):
    """
    Inputs: G {[adj list]} -- directed graph represented as an adjacency list adj:
                        adj[1] is a list containing the neighbors of vertex 1
                        (by default, vertices are numbered from 0 to |V| - 1)
    Output: G {[adj list]} -- reversed directed graph

    """
    reversed_list = [[] for _ in G]
    for begin, ends in enumerate(G):
        for end in ends:
            reversed_list[end] += [begin]
    return reversed_list

def bfs(G, s, bool):
    """
    Inputs: G {[adj list]} -- directed graph represented as an adjacency list adj:
                        adj[1] is a list containing the neighbors of vertex 1
                        (by default, vertices are numbered from 0 to |V| - 1)
            s {[vertex]} --  a vertex_s in 0,...,|V|-1

    Output:  For all u reachable from s, dist(u) is set to the distance from s to u

    """

    dist = list()
    prev = list()
    for u in range(0,len(G)):
        dist.append(-999)
        prev.append(0)
    
    dist[s] = 0
    q = queue.Queue()
    q.put(s)

    if(bool):
        G = reverse(G)
    while (not q.empty()):
        u = q.get()
        for edge in G[u]:
            if (dist[edge] == -999):
                q.put(edge)
                dist[edge] = dist[u]+1
                prev[edge] = u

    return dist

def shortest_v_paths(adj, vertex_s):
    """
    Input:  1) A directed graph represented as an adjacency list adj:
                adj[1] is a list containing the neighbors of vertex 1
                (by default, vertices are numbered from 0 to |V| - 1)
            2) a vertex_s in 0,...,|V|-1

    Output: a matrix of distances, where M[i,j] gives the length of the shortest
            path from vertex i to vertex j passing through vertex_s
    """
    # < your code goes here.  feel free to change the name of the variables.
    # can represent pairwise distances as a list of lists,
    # where dist[i][j] gives distance from i to j

    # example to demonstrate list of lists (you can delete)
    num_vertices = len(adj)
    dist = []
    row = []
    i_to_s = bfs(adj, vertex_s, 1)
    s_to_j = bfs(adj, vertex_s, 0)

    for i in i_to_s:
        for j in s_to_j:
            row.append(i + j)
        dist.append(row)
        row = []
    return dist


def main():
    """
    A simple test case for your algorithm with four vertices 0,1,2,3
    """
    adj = [[1],
           [2, 3],
           [1, 3],
           [0]]

    print(np.matrix(shortest_v_paths(adj, 3)))
    # print(np.matrix(shortest_v_paths(adj, 2)))
    # In this graph, 
    # 1) the shortest path from 3 to 3 passing through 3 has length 0
    # 2) The shortest path from 0 to 2 passing through 3 is
    # 0 -> 1 -> 3 -> 0 -> 1 -> 2 having a length of 5

if __name__ == '__main__':
    main()
