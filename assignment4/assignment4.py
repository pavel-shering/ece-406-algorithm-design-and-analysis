#!/usr/bin/env python3
"""
ECE406, Winter 2018
Assignment #6, Q6
"""

def make_change(coins, change_value):
    """
    Input: a list coins[1 ... n]  where coins[i] contains value of ith coin
           an integer change_value that we want to make change for
    Output: a list coins_used[1 ... n] where coins_used[i] gives number of coins 
            of value coins[i] used to make change, or False if change cannot be made
    """
    coins_empty = [0]*len(coins)
    change = []
    change.append(coins_empty)

    if change_value == 0:
        return change[0]    

    for value in range(1, change_value + 1):
        change.append(coins_empty)

        for coin in range(len(coins)):
            if coins[coin] <= value:
                tmp = value - coins[coin]

                if (tmp == 0) or (sum(change[tmp]) != 0):
                    if ((sum(change[value]) == 0 or sum(change[value]) > (sum(change[tmp]) + 1))):
                        change[value] = change[tmp][:]
                        change[value][coin] += 1

    if sum(change[change_value]) > 0:
        return change[change_value]
    else:
        return False

def main():
    """
    Testing your change maker
    """
    print(make_change([2, 7, 13, 29], 335))
    print(make_change([1, 5, 10, 24], 737))

if __name__ == '__main__':
    main()
